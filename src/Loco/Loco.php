<?php

namespace Affilicon\Loco;

use Illuminate\Support\Facades\Cache;

class Loco implements LocoContract
{
    /**
     * A string that should be prepended to cache kys.
     */
    const CACHE_PREFIX = 'LOCO';

    /**
     * localise.biz api url.
     *
     * @var string
     */
    protected $api;

    /**
     * Available projects.
     *
     * @var array
     */
    protected $projects;

    /**
     * The {index} parameter specifies whether the translations in your
     * file are indexed by asset IDs or source texts.
     *
     * @var string
     */
    protected $index;

    /**
     * Available languages.
     *
     * @var array
     */
    protected $languages;

    /**
     * Loco constructor.
     * @param $config
     */
    public function __construct($config)
    {
        $this->api = $config['api'];
        $this->projects = $config['projects'];
        $this->index = $config['default_loco_index'];
        $this->languages = $config['languages'];
    }

    /**
     * Get assets from cache. If, for the very first time,
     * items are not available in the specified cache, load
     * assets from api url and store into cache.
     *
     * @param $project
     * @param $language
     * @param null $locoIndex
     * @return mixed
     */
    public function fetch($project, $language, $locoIndex = null)
    {
        // default fallback
        $locoIndex = $locoIndex ?: $this->index;

        $cacheKey = $this->buildCacheKey($project, $language, $locoIndex);
        return Cache::rememberForever($cacheKey, function () use ($project, $language, $locoIndex) {
            return $this->getLocoAssets($project, $language, $locoIndex);
        });
    }

    /**
     * Flush all cached language objects from project and
     * refill cache with fresh assets. Try to load asset from
     * loco before flushing cache. If loco api is not available,
     * keep legacy assets in cache and throw exception.
     *
     * @param $project
     * @param null $locoIndex
     * @return mixed|void
     * @throws \Exception
     */
    public function flush($project, $locoIndex = null)
    {
        // default fallback
        $locoIndex = $locoIndex ?: $this->index;

        foreach ($this->languages as $language) {
            try {
                $assets = $this->getLocoAssets($project, $language, $locoIndex);

                if (false === $assets) {
                    throw new \Exception('Could not load assets from "' . $this->api . '"".');
                }

                Cache::forget($this->buildCacheKey($project, $language, $locoIndex));
                $cacheKey = $this->buildCacheKey($project, $language, $locoIndex);
                Cache::rememberForever($cacheKey, function () use ($assets) {
                    return $assets;
                });
            } catch (\Exception $e) {
                // do nothing, assets not provided
            }
        }
    }

    /**
     * Generate unique cache key.
     *
     * @param $project
     * @param $language
     * @param $locoIndex
     * @return string
     */
    protected function buildCacheKey($project, $language, $locoIndex)
    {
        return self::CACHE_PREFIX . $project . $language . $locoIndex;
    }

    /**
     * Get loco api key from config array.
     *
     * @param $project
     * @return string|mixed
     * @throws \Exception
     */
    protected function getApiKey($project)
    {
        if (!array_key_exists($project, $this->projects)) {
            throw new \Exception('Project with name "' . $project . '" is not configured in config/loco.php.');
        }

        return $this->projects[$project];
    }

    /**
     * Fetch loco assets from url.
     *
     * @param $project
     * @param $language
     * @return bool|string
     */
    protected function getLocoAssets($project, $language, $locoIndex)
    {
        $requestUrl = $this->api . $language . '.json?key=' . $this->getApiKey($project) . '&index=' . $locoIndex;
        return file_get_contents($requestUrl);
    }
}
