<?php

namespace Affilicon\Loco;

interface LocoContract
{
    /**
     * @param $project
     * @param $language
     * @param null $locoIndex
     * @return mixed
     */
    public function fetch($project, $language, $locoIndex = null);

    /**
     * @param $project
     * @param null $locoIndex
     * @return mixed
     */
    public function flush($project, $locoIndex = null);
}
